# whoami

## 简介
基于HttpServer，自定义ClassLoader，可动态的添加接口的HTTP服务器

### mvn archetype

```
mvn archetype:generate
cd ./
mvn clean
mvn archetype:create-from-project
cd target/generated-sources/archetype/
mvn install
mvn archetype:crawl
mvn archetype:generate -X -DarchetypeCatalog=local
```
### mvn 编译

```
mvn help:system

mvn archetype:generate -X -DarchetypeCatalog=local

mvn test

mvn clean install  -Ddockerfile.skip

mvn package -DskipTests -Ddockerfile.skip
```

### jar 运行

```
java -jar  whoami-latest-bin.jar
java -jar  whoami-latest-bin.jar 8000

```