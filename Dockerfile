FROM openjdk:8-jdk-alpine
RUN echo "Asia/Shanghai" > /etc/timezone
WORKDIR /usr/src
ADD target/whoami-bin.jar /usr/src/main.jar
CMD ["java","-jar","/usr/src/main.jar"]