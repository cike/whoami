package cn.plem;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.Path;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

@Path("/ping")
public class PingHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		exchange.sendResponseHeaders(200, 0);
		try (OutputStream out = exchange.getResponseBody()) {
			out.write("pong".getBytes());
		}
	}

}
